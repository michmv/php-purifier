<?php

namespace MMV\Parser\Purifier\Types;

abstract class Entity
{
    public string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
