<?php

namespace MMV\Parser\Purifier\Types;

use MMV\Parser\Purifier\Types\Entity;

class Allow extends Entity
{
    public array $attributes = [];

    /**
     * @param string $name
     * @param array $attributes [ \MMV\Parser\Purifier\Types\Entity ]
     */
    public function __construct(string $name, array $attributes=[])
    {
        parent::__construct($name);
        $this->attributes = $attributes;
    }
}
