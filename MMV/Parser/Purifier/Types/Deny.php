<?php

namespace MMV\Parser\Purifier\Types;

use DOMElement;
use MMV\Parser\Purifier\Types\Entity;

class Deny extends Entity
{
    public function alarm(DOMElement $element, string $str)
    {
        // ...
    }
}
