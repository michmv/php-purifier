<?php

namespace MMV\Parser\Purifier\Types;

use MMV\Parser\Purifier\Types\Allow;
use DOMElement;

class AllowAttr extends Allow
{
    public ?string $value = null;

    public function __construct(string $name)
    {
        parent::__construct($name);
    }

    public function checkAttributeValue(string $name, string $value): bool
    {
        // ...
        return true;
    }

    public function alarm(DOMElement $element, string $str)
    {
        // ...
    }
}