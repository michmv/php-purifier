<?php

namespace MMV\Parser\Purifier;

use Masterminds\HTML5 as HTML5Parser;
use DOMComment;
use DOMElement;
use DOMText;
use DOMNode;
use MMV\Parser\Purifier\PurifierException;
use MMV\Parser\Purifier\Types\Entity;
use MMV\Parser\Purifier\Types\Allow;
use MMV\Parser\Purifier\Types\AllowAttr;
use MMV\Parser\Purifier\Types\Deny;

class Html5
{
    public const ShowErrors = 1;

    public const CommentsAllowed = 2;

    public const BadTagToText = 4;

    public const BadAttrToText = 8;

    public const ListVoidTag = ['area', 'base', 'br', 'col', 'embed', 'hr', 'img', 'input', 'link', 'meta', 'param', 'source', 'track', 'wbr'];

    /**
     * @param string $str
     * @param array $filters [\MMV\Parser\Purifier\Types\Entity]
     * @param integer $flags
     * @return array
     */
    public static function purifier(string $str, array $filters, int $flags=0): array
    {
        $html5 = new HTML5Parser();
        $dom = $html5->loadHTMLFragment($str);

        return static::collapseText(static::domToArray($dom, $filters, $flags, $str));
    }

    public static function toHtml(array $arr, ?array $attrFilters=null): string
    {
        if($attrFilters === null) {
            $urlEncode = function($str) { return urlencode($str); };
            $attrFilters = [
                'src'  => $urlEncode,
                'href' => $urlEncode,
                'action' => $urlEncode,
            ];
        }

        return static::arrayToHtml($arr, $attrFilters);
    }

    protected static function domToArray(DOMNode $dom, array $filters, int $flags, string &$str): array
    {
        $res = [];

        foreach($dom->childNodes as $node) {
            if($node instanceof DOMElement) {
                $res = array_merge($res, static::elementToArray($node, $filters, $flags, $str));

            } else if($node instanceof DOMComment) {

                if(static::CommentsAllowed & $flags) {
                    $res[] = ['type'=>'comment', 'value'=>$node->nodeValue];

                } else {
                    if(static::ShowErrors & $flags)
                        throw PurifierException::make('Comment don\'t allowed', 2);

                    if(static::BadTagToText & $flags)
                        $res[] = static::makeText('<!--'.$node->nodeValue.'-->');
                }
            } else if($node instanceof DOMText) {
                $res[] = static::makeText($node->nodeValue);

            } else {
                throw PurifierException::make('I can\'t process the DOM class', 1, [get_class($node)]);
            }
        }

        return $res;
    }

    protected static function elementToArray(DOMElement $element, array $filters, int $flags, string &$str): array
    {
        if($filter = static::findFilterByName($element->tagName, $filters)) {
            if($filter instanceof Allow) {
                $response = static::extractAttributs($element, $filter->attributes, $flags, $str);
                if($response[0]) { // create tag
                    $child = static::domToArray($element, $filters, $flags, $str);
                    return [
                        [
                            'type' => 'tag',
                            'name' => $element->tagName,
                            'void' => ((static::elementIsVoid($element->tagName)) ? true : false),
                            'attrs' => $response[1],
                            'child' => $child,
                        ]
                    ];
                }
            }
            else if($filter instanceof Deny) {
                $filter->alarm($element, $str);

                if(static::ShowErrors & $flags) // tag is deny
                    throw PurifierException::make('Tag don\'t allowed', 4, [$element->tagName]);

            } else {
                throw PurifierException::make('I can\'t process the Filter tag', 3, [get_class($filter)]);
            }
        } else {
            if(static::ShowErrors & $flags) // not found filter for this tag
                throw PurifierException::make('Tag don\'t allowed', 4, [$element->tagName]);
        }

        // bad tag to text or ignore
        if(static::BadTagToText & $flags) { // tag to text
            return static::elementToText($element, $filters, $flags, $str);
        }
        else if(count($element->childNodes)) { // tag ignore but has child
            return static::domToArray($element, $filters, $flags, $str);
        }
        else {
            return []; // tag ignore
        }
    }

    /**
     * @param \DOMElement $element
     * @param array $filters [\MMV\Parser\Purifier\Types\Entity]
     * @param integer $flags
     * @param string $str
     * @return array [statuc, [nameAttribut => valueAttribute]]
     */
    protected static function extractAttributs(DOMElement $element, array $filters, int $flags, string &$str): array
    {
        $res = [];
        foreach($element->attributes as $attribut) {
            if($filter = static::findFilterByName($attribut->name, $filters)) {
                if($filter instanceof AllowAttr) {
                    if($filter->checkAttributeValue($attribut->name, $attribut->value)) {
                        $value = $filter->value !== null ? $filter->value : $attribut->value;
                        $res[$attribut->name] = $value;
                    }
                    else {
                        $filter->alarm($element, $str);

                        if(static::ShowErrors & $flags) // show error
                            throw PurifierException::make('Attribute don\'t allowed', 5, [$attribut->name, $element->tagName]);

                        if(static::BadAttrToText & $flags) // tag to text
                            return [false, []];
                    }
                }
                else if($filter instanceof Deny) {
                    $filter->alarm($element, $str);

                    if(static::ShowErrors & $flags) // show error
                        throw PurifierException::make('Attribute don\'t allowed', 5, [$attribut->name, $element->tagName]);

                    if(static::BadAttrToText & $flags) // tag to text
                        return [false, []];
                }
                else {
                    throw PurifierException::make('I can\'t process the Filter attribute', 6, [get_class($filter)]);
                }
            }
            else {
                if(static::ShowErrors & $flags) // show error
                    throw PurifierException::make('Attribute don\'t allowed', 5, [$attribut->name, $element->tagName]);

                if(static::BadAttrToText & $flags) // tag to text
                    return [false, []];
            }
        }
        return [true, $res];
    }

    protected static function elementToText(DOMElement $element, array $filters, int $flags, string &$str): array
    {
        $res[] = static::makeText(static::tagToText($element));

        if(count($element->childNodes)) {
            $res = array_merge($res, static::domToArray($element, $filters, $flags, $str));
            $res[] = static::makeText('</'.$element->tagName.'>');
        }
        else if(!static::elementIsVoid($element->tagName)) {
            $res[] = static::makeText('</'.$element->tagName.'>');
        }

        return $res;
    }

    protected static function elementIsVoid(string $tag): bool
    {
        return in_array($tag, static::ListVoidTag);
    }

    protected static function tagToText(DOMElement $element): string
    {
        $attrs = [];
        foreach($element->attributes as $it) {
            $qm = strpos($it->value, '"') === false ? '"' : '\'';
            $attrs[] = $it->name.'='.$qm.$it->value.$qm;
        }

        return '<'.$element->tagName.
            (($attrs) ? ' '.implode(' ', $attrs) : '').
            '>';
    }

    protected static function findFilterByName(string $name, array $filters): ?Entity
    {
        foreach($filters as $filter) {
            if($filter->getName() === $name) return $filter;
        }
        return null;
    }

    protected static function makeText(string $text): array
    {
        return ['type'=>'text', 'value'=>$text];
    }

    protected static function collapseText(array $entities): array
    {
        $prev = -1;
        $count = count($entities);
        for($i=0; $i < $count; $i++) {
            if($prev >= 0 && $entities[$i]['type'] === 'text' && $entities[$prev]['type'] === 'text') {
                $entities[$prev]['value'] = $entities[$prev]['value'] . $entities[$i]['value'];
                unset($entities[$i]);
            }
            else {
                if($entities[$i]['type'] === 'tag')
                    $entities[$i]['child'] = static::collapseText($entities[$i]['child']);
                $prev = $i;
            }
        }
        return array_values($entities);
    }

    protected static function arrayToHtml(array $arr, $attrFilters): string
    {
        $res = '';
        foreach($arr as $i) {
            if($i['type'] == 'text') {
                $res .= htmlspecialchars($i['value'], ENT_QUOTES, 'UTF-8', true);
            }
            else if($i['type'] == 'comment') {
                $res .= '<!--'.$i['value'].'-->';
            }
            else if($i['type'] == 'tag') {
                $res .= '<'.$i['name'];
                $res .= ($t = static::attrsToHtml($i['attrs'], $attrFilters)) ? ' '.$t : '';
                $res .= '>';
                $res .= static::arrayToHtml($i['child'], $attrFilters);
                $res .= $i['void'] ? '' : '</'.$i['name'].'>';
            }
            else {
                throw PurifierException::make('Unknown type', 7, [$i['type']]);
            }
        }
        return $res;
    }

    protected static function attrsToHtml(array $attrs, array $filters): string
    {
        $res = [];
        foreach($attrs as $name => $value) {
            if(array_key_exists($name, $filters)) {
                $func = $filters[$name];
                $value = call_user_func($func, $value);
            }
            $qm = strpos($value, '"') === false ? '"' : '\'';
            $res[] = $name.'='.$qm.$value.$qm;
        }

        return implode(' ', $res);
    }
}
