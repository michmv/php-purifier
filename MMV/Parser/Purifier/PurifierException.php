<?php

namespace MMV\Parser\Purifier;

use Exception;

class PurifierException extends Exception
{
    public array $parameters = [];

    public static function make(string $message, int $code, array $parameters=[])
    {
        $e = new static($message, $code);
        $e->parameters = $parameters;
        return $e;
    }
}
