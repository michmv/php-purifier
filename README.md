# Parsing html5 to an array of entities and back.

## Usage

### Base

```php
<?php
use MMV\Parser\Purifier\Html5;
use MMV\Parser\Purifier\Types\AllowAttr;
use MMV\Parser\Purifier\Types\Allow;

$filters = [
    new Allow('a', [ new AllowAttr('href') ]),
    new Allow('img', [ new AllowAttr('src') ]),
    new Allow('div'),
    new Allow('br'),
];

$str = <<< 'HERE'
<div style="font-size:100px;">
    <p>
        <a href="url link">Link</a><br>
        <img src="url link">
    </p>
</div>
HERE;

$arr = Html5::purifier($str, $filters);

var_dump(Html5::toHtml($arr));
/*
<div>
    
        <a href="url+link">Link</a><br>
        <img src="url+link">
    
</div>
```

### Get error

```php
<?php
use MMV\Parser\Purifier\PurifierException;

function validHtml(string $str, $filters, $flags): array {
    try {
        Html5::purifier($str, $filters, $flags);
        return [];
    } catch (PurifierException $e) {
        return [ $e->getMessage() . (count($e->parameters) ? '|'.implode(',', $e->parameters) : '') ];
    }
}

var_dump(validHtml('<p>test</p>', $filters, Html5::ShowErrors));
// Tag don't allowed|p
```

### Settings

```php
<?php
$flags = Html5::ShowErrors |
         Html5::CommentsAllowed |
         Html5::BadTagToText |
         Html5::BadAttrToText;

Html5::purifier($str, $filters, $flags);
```

# License

MIT
