<?php

namespace Tests;

use DOMElement;
use PHPUnit\Framework\TestCase;
use MMV\Parser\Purifier\Html5;
use MMV\Parser\Purifier\PurifierException;
use MMV\Parser\Purifier\Types\Allow;
use MMV\Parser\Purifier\Types\Deny;
use MMV\Parser\Purifier\Types\AllowAttr;

class AttrTest extends TestCase
{
    public function getFilter1()
    {
        return [
            new Allow('div', [
                new AllowAttr('class'),
            ]),
        ];
    }

    public function getFilter2()
    {
        $deny = new class($this, 'id') extends Deny {
            public function __construct($test, $name) {
                $this->test = $test;
                parent::__construct($name);
            }
            public function alarm(DOMElement $element, string $str) {
                $this->test->assertEquals('div', $element->tagName);
                $this->test->assertEquals('<div id="bad"></div>', $str);
            }
        };

        return [
            new Allow('div', [
                $deny,
            ]),
        ];
    }

    public function getFilter3()
    {
        $allow = new class($this, 'id') extends AllowAttr {
            public function __construct($test, $name) {
                $this->test = $test;
                parent::__construct($name);
            }
            public function checkAttributeValue(string $name, string $value): bool {
                return false;
            }
            public function alarm(DOMElement $element, string $str) {
                $this->test->assertEquals('div', $element->tagName);
                $this->test->assertEquals('<div id="bad2"></div>', $str);
            }
        };

        return [
            new Allow('div', [
                $allow,
            ]),
        ];
    }

    /**************************************************************************** */

    public function testAttribute0()
    {
        $res = Html5::purifier('<DIV CLASS="test" onclick="alert(\'bad\');">simple</div>', $this->getFilter1(), 0);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>['class'=>'test'], 'child'=>[
                ['type'=>'text', 'value'=>'simple'],
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testAttribute1()
    {
        try {
            Html5::purifier('<DIV CLASS="test" onclick="alert(\'bad\');">simple</div>', $this->getFilter1(), 1);
            $this->assertTrue(false);
        } catch (PurifierException $e) {
            $this->assertEquals(5, $e->getCode());
            $this->assertEquals('Attribute don\'t allowed', $e->getMessage());
            $this->assertEquals(['onclick', 'div'], $e->parameters);
        }
    }

    public function testAttribute4()
    {
        $res = Html5::purifier('<DIV CLASS="test" onclick="alert(\'bad\');">simple</div>', $this->getFilter1(), 4);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>['class'=>'test'], 'child'=>[
                ['type'=>'text', 'value'=>'simple'],
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testAttribute4BadAttrToText()
    {
        $res = Html5::purifier('<DIV CLASS="test" onclick="alert(\'bad\');">simple</div>', $this->getFilter1(), 12);

        $expected = [
            ['type'=>'text', 'value'=>'<div class="test" onclick="alert(\'bad\');">simple</div>'],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testAttribute5()
    {
        try {
            Html5::purifier('<DIV CLASS="test" onclick="alert(\'bad\');">simple</div>', $this->getFilter1(), 5);
            $this->assertTrue(false);
        } catch (PurifierException $e) {
            $this->assertEquals(5, $e->getCode());
            $this->assertEquals('Attribute don\'t allowed', $e->getMessage());
            $this->assertEquals(['onclick', 'div'], $e->parameters);
        }
    }

    /**************************************************************************** */

    public function testAttributeDeny()
    {
        $res = Html5::purifier('<div></div>', $this->getFilter2(), 0);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testAttributeDeny2()
    {
        Html5::purifier('<div id="bad"></div>', $this->getFilter2(), 0);
    }

    public function testAttributeAllowAlarm()
    {
        $res = Html5::purifier('<div></div>', $this->getFilter3(), 0);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testAttributeAllowAlarm2()
    {
        Html5::purifier('<div id="bad2"></div>', $this->getFilter3(), 0);
    }

    /**************************************************************************** */

    public function testQuotationMarks()
    {
        $res = Html5::purifier('<div id="bad2" data-test1="tex\'t" data-test2=\'tex"t\'></div>', [], 4);

        $expected = [
            ['type'=>'text', 'value'=>'<div id="bad2" data-test1="tex\'t" data-test2=\'tex"t\'></div>'],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testDefaultValue()
    {
        $attr2 = new AllowAttr('attr2');
        $attr2->value = '1';

        $filters = [
            new Allow('div', [
                new AllowAttr('attr1'),
                $attr2,
                new AllowAttr('attr3'),
                new AllowAttr('attr4'),
            ]),
        ];
        $res = Html5::purifier('<div attr1 attr2 attr3="" attr4="..."></div>', $filters, 0);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'child'=>[], 'attrs'=>[
                'attr1' => '', 'attr2' => '1', 'attr3' => '', 'attr4' => '...',
            ]],
        ];

        $this->assertEquals($expected, $res);
    }
}
