<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use MMV\Parser\Purifier\Html5;
use MMV\Parser\Purifier\Types\Allow;
use MMV\Parser\Purifier\Types\AllowAttr;

class ToHtmlTest extends TestCase
{
    public function testString1()
    {
        $str = '<div class="test">simple</div><!--comment--><input value=\'text "quote"\'><a href="url to page">link</a><img src="url to image">finish';
        $filters = [
            new Allow('div',   [new AllowAttr('class')]),
            new Allow('input', [new AllowAttr('value')]),
            new Allow('a',     [new AllowAttr('href')]),
            new Allow('img',   [new AllowAttr('src')]),
        ];
        $res = Html5::purifier($str, $filters, 2);

        $expected = '<div class="test">simple</div><!--comment--><input value=\'text "quote"\'><a href="url+to+page">link</a><img src="url+to+image">finish';

        $this->assertEquals($expected, Html5::toHtml($res));
    }
}