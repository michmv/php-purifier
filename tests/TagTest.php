<?php

namespace Tests;

use DOMElement;
use PHPUnit\Framework\TestCase;
use MMV\Parser\Purifier\Html5;
use MMV\Parser\Purifier\PurifierException;
use MMV\Parser\Purifier\Types\Allow;
use MMV\Parser\Purifier\Types\Deny;

class TagTest extends TestCase
{
    public function getFilter1()
    {
        return [
            new Allow('div'),
        ];
    }

    public function getFilter2()
    {
        return [
            new Allow('div'),
            new Allow('span'),
        ];
    }

    /**************************************************************************** */

    public function testTag()
    {
        $res = Html5::purifier('<div>simple</div>', $this->getFilter1(), 0);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'text', 'value'=>'simple']
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testTagSub()
    {
        $res = Html5::purifier('<div><div>simple</div></div>', $this->getFilter1(), 0);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                    ['type'=>'text', 'value'=>'simple'],
                ]],
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testTagBad()
    {
        $res = Html5::purifier('<div>simple</div>', [], 0);

        $expected = [
            ['type'=>'text', 'value'=>'simple'],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testTagBadSub()
    {
        $res = Html5::purifier('<div><p>simple</p></div>', [], 0);

        $expected = [
            ['type'=>'text', 'value'=>'simple'],
        ];

        $this->assertEquals($expected, $res);
    }

    /**************************************************************************** */

    public function testTag2_0()
    {
        $res = Html5::purifier('<div><p><span>simple</span></p></div>', $this->getFilter2(), 0);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'tag', 'name'=>'span', 'void'=>false, 'attrs'=>[], 'child'=>[
                    ['type'=>'text', 'value'=>'simple'],
                ]],
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testTag2_1()
    {
        try {
            Html5::purifier('<div><p><span>simple</span></p></div>', $this->getFilter2(), 1);
            $this->assertTrue(false);
        } catch (PurifierException $e) {
            $this->assertEquals(4, $e->getCode());
            $this->assertEquals('Tag don\'t allowed', $e->getMessage());
            $this->assertEquals(['p'], $e->parameters);
        }
    }

    public function testTag2_2()
    {
        $res = Html5::purifier('<div><p><span>simple</span></p></div>', $this->getFilter2(), 2);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'tag', 'name'=>'span', 'void'=>false, 'attrs'=>[], 'child'=>[
                    ['type'=>'text', 'value'=>'simple'],
                ]],
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testTag2_3()
    {
        try {
            Html5::purifier('<div><p><span>simple</span></p></div>', $this->getFilter2(), 3);
            $this->assertTrue(false);
        } catch (PurifierException $e) {
            $this->assertEquals(4, $e->getCode());
            $this->assertEquals('Tag don\'t allowed', $e->getMessage());
            $this->assertEquals(['p'], $e->parameters);
        }
    }

    public function testTag2_4()
    {
        $res = Html5::purifier('<div><p><span>simple</span></p></div>', $this->getFilter2(), 4);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'text', 'value'=>'<p>'],
                ['type'=>'tag', 'name'=>'span', 'void'=>false, 'attrs'=>[], 'child'=>[
                    ['type'=>'text', 'value'=>'simple'],
                ]],
                ['type'=>'text', 'value'=>'</p>'],
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testTag2_5()
    {
        try {
            Html5::purifier('<div><p><span>simple</span></p></div>', $this->getFilter2(), 5);
            $this->assertTrue(false);
        } catch (PurifierException $e) {
            $this->assertEquals(4, $e->getCode());
            $this->assertEquals('Tag don\'t allowed', $e->getMessage());
            $this->assertEquals(['p'], $e->parameters);
        }
    }

    public function testTag2_6()
    {
        $res = Html5::purifier('<div><p><span>simple</span></p></div>', $this->getFilter2(), 6);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'text', 'value'=>'<p>'],
                ['type'=>'tag', 'name'=>'span', 'void'=>false, 'attrs'=>[], 'child'=>[
                    ['type'=>'text', 'value'=>'simple'],
                ]],
                ['type'=>'text', 'value'=>'</p>'],
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testTag2_7()
    {
        try {
            Html5::purifier('<div><p><span>simple</span></p></div>', $this->getFilter2(), 7);
            $this->assertTrue(false);
        } catch (PurifierException $e) {
            $this->assertEquals(4, $e->getCode());
            $this->assertEquals('Tag don\'t allowed', $e->getMessage());
            $this->assertEquals(['p'], $e->parameters);
        }
    }

    /**************************************************************************** */

    public function testCollapseText()
    {
        $res = Html5::purifier('<div>simple</div>', [], 4);

        $expected = [
            ['type'=>'text', 'value'=>'<div>simple</div>'],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testCollapseText2()
    {
        $res = Html5::purifier('<div><p>simple</p></div>', [], 4);

        $expected = [
            ['type'=>'text', 'value'=>'<div><p>simple</p></div>'],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testCollapseText3()
    {
        $res = Html5::purifier('<div><p>simple</p></div>', $this->getFilter1(), 4);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'text', 'value'=>'<p>simple</p>'],
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    /**************************************************************************** */

    public function testComment()
    {
        $res = Html5::purifier('<div><!--<p>simple</p>--></div>', $this->getFilter1(), 0);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testCommentDenyError()
    {
        try {
            Html5::purifier('<div><!--<p>simple</p>--></div>', $this->getFilter1(), 1);
            $this->assertTrue(false);
        } catch (PurifierException $e) {
            $this->assertEquals(2, $e->getCode());
            $this->assertEquals('Comment don\'t allowed', $e->getMessage());
        }
    }

    public function testCommentAllow()
    {
        $res = Html5::purifier('<div><!--<p>simple</p>--></div>', $this->getFilter1(), 2);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'comment', 'value'=>'<p>simple</p>']
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testCommentDenyBadToText()
    {
        $res = Html5::purifier('<div><!--<p>simple</p>--></div>', $this->getFilter1(), 4);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'text', 'value'=>'<!--<p>simple</p>-->']
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    /**************************************************************************** */

    public function testDenyTag()
    {
        $deny = new class($this, 'script') extends Deny {
            public function __construct($test, $name) {
                $this->test = $test;
                parent::__construct($name);
            }
            public function alarm(DOMElement $element, string $str)
            {
                $this->test->assertEquals('script', $element->tagName);
                $this->test->assertEquals('<div><script>//alarm</script></div>', $str);
            }
        };

        $filters = [
            new Allow('div'),
            $deny,
        ];

        Html5::purifier('<div><script>//alarm</script></div>', $filters, 0);
    }

    public function testDenyTag2()
    {
        $filters = [
            new Allow('div'),
            new Deny('script'),
        ];

        $res = Html5::purifier('<div><script>//alarm</script></div>', $filters, 0);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'text', 'value'=>'//alarm']
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testDenyTag3()
    {
        $filters = [
            new Allow('div'),
            new Deny('script'),
        ];

        $res = Html5::purifier('<div><script>//alarm</script></div>', $filters, 4);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'text', 'value'=>'<script>//alarm</script>']
            ]],
        ];

        $this->assertEquals($expected, $res);
    }

    public function testDenyTag4()
    {
        $filters = [
            new Allow('div'),
            new Deny('script'),
        ];

        $res = Html5::purifier('<div><script><script</script><script>>//alarm</script</script><script>></script></div>', $filters, 0);

        $expected = [
            ['type'=>'tag', 'name'=>'div', 'void'=>false, 'attrs'=>[], 'child'=>[
                ['type'=>'text', 'value'=>'<script>//alarm</script>']
            ]],
        ];

        $this->assertEquals($expected, $res);
    }
}
